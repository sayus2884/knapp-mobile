import React, { Component } from 'react';
import { Font } from "expo";
import Meteor from 'react-native-meteor';
import { Text, View, StatusBar } from 'react-native';
import { Container, Header, Root } from 'native-base';
import { StackNavigator } from 'react-navigation';

// Navigator
import AppNavigator from './AppNavigator'

export default class App extends Component {

   constructor(){
      super();

      this.state = {
         fontLoaded: false,
      };
   }

   render() {

      const statusBarHeight = StatusBar.currentHeight

      return !this.state.fontLoaded ? (
         <Container></Container>
      ) : (
         <Root>
            <View style={{ height: statusBarHeight, backgroundColor: 'black' }}></View>

            <AppNavigator />
         </Root>
      )
   }

   async componentDidMount() {
      await Font.loadAsync({
         'Roboto_medium': require('./assets/fonts/Roboto-Medium.ttf'),
      });

       this.setState({ fontLoaded: true });
   }
}
