import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import Meteor, { createContainer } from 'react-native-meteor';
import { NavigationActions } from 'react-navigation';
import { Container, Header, Content, Form, Item, Label, Input, Button, Text } from 'native-base';

class Login extends Component {
   constructor(){
      super();

      this.state = {
         username: '',
         password: ''
      }

      this._handleSignIn = this._handleSignIn.bind(this);
   }

   render() {

      return (
         <Container style={{backgroundColor: "#FFF"}}>

            <Header>
            </Header>

            <Content>
               <Form>
                  <Item>
                     <Input placeholder="Username"
                        onChangeText={(username) => this.setState({username})}
                        />
                  </Item>
                  <Item last>
                     <Input placeholder="Password"
                        secureTextEntry={true}
                        onChangeText={(password) => this.setState({password})}/>
                  </Item>
               </Form>

               <Button title="Go to EstablishmentList" block style={{ margin: 15, marginTop: 50 }} onPress={this._handleSignIn}>
                  <Text>Sign In</Text>
               </Button>

            </Content>
         </Container>
      );
   }

   componentDidMount(){

      console.log(Meteor.userId());

      if (Meteor.userId()) {
         this._handleLoginFlow();
      }
   }


   _handleSignIn(){
      const { navigate } = this.props.navigation;
      const { username, password } = this.state;

      Meteor.loginWithPassword(username, password, (err) => {
         if (!err){
            this._handleLoginFlow();
         } else {
            console.log(err);
         }
      });

   }

   _handleLoginFlow(){
      const { dispatch } = this.props.navigation;
      const resetStack = NavigationActions.reset({
         index: 0,
         actions: [
            NavigationActions.navigate({ routeName: 'EstablishmentList'})
         ]
      });

      dispatch(resetAction);
   }
}

export default Login
