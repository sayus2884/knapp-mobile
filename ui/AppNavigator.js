import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Login from './pages/Login';
import EstablishmentList from './pages/EstablishmentList';

// Create Navigator
export default AppNavigator = StackNavigator({
   Login: { screen: Login },
   EstablishmentList: { screen: EstablishmentList }

}, {
   initialRouteName: 'Login',
   headerMode: 'none'
});
